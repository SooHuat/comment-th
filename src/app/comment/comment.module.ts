import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import {NgbPaginationModule, NgbAlertModule} from '@ng-bootstrap/ng-bootstrap';

import { CommentComponent } from './list/comment.component';
import { CommentDetailComponent } from './detail/comment-detail.component';
import { CommentService } from 'src/shared/services/comment.service';
import { FormsModule } from '@angular/forms';

const routes: Routes = [
    {
      path: '',
      component: CommentComponent
    },
    {
      path: 'comment',
      component: CommentComponent
    },
    {
      path: 'comment/:id',
      component: CommentDetailComponent
    }
  ];

@NgModule({
  declarations: [
    CommentComponent,
    CommentDetailComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BrowserModule,
    RouterModule,
    HttpClientModule,
    NgbPaginationModule, 
    NgbAlertModule,
    RouterModule.forRoot(routes)
  ],
  providers: [CommentService],
  bootstrap: [
    CommentComponent,
    CommentDetailComponent
  ]
})
export class CommentModule { }
