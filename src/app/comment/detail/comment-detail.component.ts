import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as _ from 'lodash';


import { CommentService } from 'src/shared/services/comment.service';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'comment-detail',
  templateUrl: './comment-detail.component.html'
})
export class CommentDetailComponent implements OnInit {
  post: any;
  comments: any;
  user: any;
  page: number = 1;
  pageSize: number = 10;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private commentService: CommentService) { }

  async ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    await forkJoin(this.commentService.getPost(id),
      this.commentService.getComments()).subscribe(result => {
        this.post = result[0];
        this.comments = result[1];
        this.user = this.commentService.getUser(this.post.userId);
        this.comments = _.filter(this.comments, comment => {
          return comment.postId == id;
        })
      });
  }

  async onBackClick() {
    this.router.navigate(['/comment']);
  }
}
